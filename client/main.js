import { Template } from 'meteor/templating';

import './main.html';

/******* Define Port *******/
const PORT = window.socketPort || 3003;
socket = require('socket.io-client')(`http://localhost:${PORT}`);

// Hack https://github.com/socketio/socket.io-client/issues/961
import Response from 'meteor-node-stubs/node_modules/http-browserify/lib/response';
if (!Response.prototype.setEncoding) {
  Response.prototype.setEncoding = function(encoding) {
    // do nothing
  }
}

// Connection
Meteor.startup(() => {
  
  socket.on('connect', function() {
    console.log('Client connected');
  });
  socket.on('disconnect', function() {
    console.log('Client disconnected');
  });
  
});

/************** Append messages on client **************/
Template.chatbox.onCreated(function(){
  socket.on('chat message', function(msg){
      $('#messages').append($('<li>').text(msg));
      window.scrollTo(0, document.body.scrollHeight);
    });
})

/************** Emit chat message to server **************/
Template.chatbox.events({
  'submit form' () {
    socket.emit('chat message', $('#m').val());    
    $('#m').val('');
    return false;
  },
});

